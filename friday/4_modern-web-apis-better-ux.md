#Modern web APIs for a better user experience ( Jad Joubran & Nicole Saidy )

@joubranjad
@nicolesaidy
!!!ASK FOR SLIDES
http://github.com/jadjourban/promyfill

##Polyfills

- Always include a polyfill.  Not a great idea
- [Polyfill.io](http://Polyfill.io) - standard way to do this
- But...polyfill.io is still problematic because of its CDN nature
- So...make it async

##Intersection Observer

We can track visibility of element performantly.
[https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API)
WICG has a polyfill

```
const io = new IntersectionObserver ( entries => {
  const entry = entries[0];
  if(entry.isIntersection){
    // visible
  }else{
    // invisible
  }
});
io.observe(element);
```

*Use case:* Infinite scrolling.

Use a _sentinel_ (an element that's not necessarily noticeable by user, but that acts as the scroll anchor)

```
const io = new IntersectionObserver( 
  entries => {
    if( !entries[0].isIntersection) return;
    loadItems(10);
    scroller.appendChild(sentinel);
  },{
    rootMargin: '20px'
  }
);
```

*Use case:* Scroll-to-top button
*Use case:* Mute button (for when video is off-screen)

##CSS Custom Properties / variables

```
:root {
  // or shadow root
  --variable-name: value;
}
p {
  color: var(--variable-name);
}
```

Since these are browser native variables, you can change them in devtools and update universally, or update via JS where appropriate.
This is *ultrafrickingcool*.
Also works with inline SVG

*Use cases*:

- Styling web components.
- DRY

*Example of cascade:*

```
a {
  --link-color: red;
  color: var(--link-color);
}
a:hover {
  --link-color: blue;
  // no need to redefine a color definition
}
```

*Colorpicker*

!!!Extrapolate from the following
```
<input id="colorpicker" type="color">
var bodyStyles = window.getComputedStyle(document.body);
var fooBar = bodyStyles.getPropertyValue('--foo-bar');
body.style.setProperty('--foo-bar', newValue);
```

##Online/Offline State

Browser support back to IE8

Easy method to disabled all controls if offline

*Example:*

```
window.addEventListener('online', onlineMode);
window.addEventListener('offline', offlineMode);
if (!navigator.online() {
  offline();
  // be sure to do reverse as well
}
```

##Cache API

Offline scenarios:

- flight w/o wifi
- deadzones
- bad network connection
- countries w/ expensive data

Create "comfortably offline" experiences.

###What it does with service workers

Listens for install event, add what you want to precache in the SW code.

*WINDOW scope*

```
if('caches' in window) {
  //progressive enhancement
  caches.open('v1-articles').then( cache => {
    return cache.put('key', new Response('hello world'));
  })
}
```

##Payment request API

!!!65.9% of users top partway through purchase on mobile devices (Google)

This is *not a provider* (duh)
Uses OS based secure storage
Pre-loads payment methods the user has already saved

##Web Share API

Gives users a way to share content to native apps

Current sharing options are not ideal

- Share buttons for specific services are bad mmkay
- Built-in browser share buttons may not be available

Share button using this API leads to OS/browser default sharing menu, which then goes to the native application

```
if(navigator.share === undefined) {
  sharebutton.hidden = true;
}
button.click( function() {
  navigator.share({
    title: '...',
    text:  '...',
    url:   '...'
  }).then(...);

});
```
