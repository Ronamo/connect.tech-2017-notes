#Web Components: LEGO Bricks of the Web
Abraham Williams
Pearl Latteier

[slides.today](http://slides.today)

##Roleplay Example
*First Spec* Display an image
Say we just want to add an image. Abraham doesn't know how, but Pearl does.
*Second spec*: What if it's not found?
So now we need an event listener for if it isn't found.
Pearl writes it in jQuery.
*New spec:* On image not found errors, ping the server
So now we must tell the server which images are broken
Pearl quits. She gets a better offer.
Pearl2 shows up. She doesn't know the codebase. So she adds a new listener. She also installs the latest jQuery.
*New Spec:* Fade in image on load
We've got nothing, so we just fade it in.
PearlII has quit, PearlIII shows up. But she writes the fade event handler in vanilla JS but doesn't remove jQuery.

---

So now we've got spaghetti (code) prepared by at least three different cooks (developers)

##Web Components (to the rescue)

A WC (Web Component) is a _reusable_ UI element.

- custom elements
- HTML imports
- HTML templates
- Shadow DOM

Imports are _dead in the water_. So we get:

- Custom elements
- JavaScript modules
- Template literals
- Shadow DOM

##Define the API

Example:

```
<better-img url="img/picture.jpg"
            fallback="img/default.jpg"
            report="log"
            fade-in="1000">
</better-img>
```

###Define

```
class BetterImg extends HTMLElement {
  constructor() {
    super(); // not strictly required, but it saves debug issues with 'this' keyword
    connectedCallback() {
      // built-in method, lifecycle event
      // once it's in the DOM
      // do expensive work here, not at construction, since it isn't used prior and may
      //  not be used
      this.innerHTML = '<img />';
      this.img.onerror = this.onerrorHandler;
      this.setSrc(this.url);
    }

  };
}
```
SEE SLIDES

##Custom Elements v1 Support
- Chrome (y)
- Firefox (uc)
- Safari (y)
- Edge (?)
- Opera (y)
- Polyfill (y)

##Distribution

JS modules

```
<head>
  <script type="module" src="better-img.js"></script>
</head>
<body>
  <better-img ...></better-img>
</body>
```

- Chrome (y)
- Firefox (uc)
- Safari (y)
- Edge (uc)
- Opera (uc)
- Polyfill (y)

##...and another new spec

*Pinterest Style Cards*

...so now PearlIX can just keep working on the same component instead of making more spaghetti.

##Global code breaks everything!

What happens when global CSS collides with component CSS?

This is where *Shadow DOM* comes into play...and kicks ass.

- Isolates DOM from CSS/JS
- Scoped, simplified CSS
- Allows composition

For composition, we can add a _slot_ ( `<slot id="cardText"></slot>` ). Allows the consumer to add content into the item.

Example of a `<slot>`:

```
<photo-card>
  <p slot="cardText">This HTML will be render in the slot.</p>
</photo-card>
```
(similar to `ng-content` from Angular. Vue uses the same nomenclature of _slot_.)

!!!SEE THE FOLLOWING LINK:
[webcomponents.org](http://webcomponents.org)
