#Non-Trival Pursuits - Learning Machines and Forgetful Humans ( Chris Heilmann - codepo8 )

[aka.ms/connect-tech](http://aka.ms/connect-tech)

##Machine Learning, Deep Learning, Artificial Intelligence, Big Data...

This is deep stuff. No, really. It's deep.

> "Given the pace of technology, I propose we leave math to the machines and go play outside." - Bill Waterman (Calvin & Hobbes)

##Big Brother is watching you?

This technology raises troubling questions. Big data has both good and bad applications.

### Big Brother is not needed.

We voluntarily hand over tons of data every single day. Worried about _what_ the government can do? Just assume that they _can_, and worry whether or not they _will_.

We live in a post-data leak world. We have been recorded and categorised.

##How do we remember and learn?

Speaker and wife decided to learn more colloquial English by playing Trivial Pursuit. Playing an old (1980's) version gave insight.

###How does learning get easier?

- Repetition
- Comparison
- Explanation
- Association

(German version of Harry Potter is really boring..."That's how sad we are.")

###Humans are weird creatures

We are capable of _complex and erratic leaps_ of understanding.
But, _we are terrible at repetition_ - as we get *bored*.
We are rubbish at adding explanations and naming things. As it is boring.
We are easily overwhelmed looking at large tasks and big amounts of data.
Computers excel at repetition and can plough in milliseconds through oodles of data.
Our job is to make sure that the data is understood correctly.
We must insure that the returned data is fit for human consumption.

###How we think of Machine Learning

We think of it in terms of sci-fi, but this fails us. We don't utilize it well.
Ubiquitous computing becomes a nuisance unless it provides a benefit.
It is easy to create a creepy, annoying chat bot. It is hard to create a good one.
AI is most effective when it enhances in the background.

##What can we use now?

- Visual recognition
- Voice recognition
- Natural language processing
- Emotion recognition
- Entity recognition

###Visual recognition

*Positives*

- Automated tagging and clustering
- Image to text / handwriting
- Alternative content (A11Y)
- Biometric login
- Automated "art direction"
- Automated moderation

*Negatives:*

- Privacy issues
- Wrong and possibly offensive automated labeling (Gorilla people of color example...used all Caucasians in training)
- False moderation and failed moderation
- Connection / upload latency
- Insufficiently trained models

###Voice Recognition

*Positives*

- Visual impairment or no screen
- Hands-free interaction (phone, car, headsets)
- Faster than typing, more natural
- Magical "Star Trek" factor

*Negatives*

- no fallbacks
- annoying
- inaccurate at times

###Emotion Recognitions

*Positives:*

- Feedback channel for product tests
- React to the most annoyed customers first
- Find happy quotes and customers to promote
- Navigate media by emotion

*Negatives:*

- False recognition results in hurtful messaging
- Quality issues can result in very wrong results
- Voice emotion recognition is still a tough nut to crack

###Entity recognition

*Positives:*

- Automated tggin and cross referencing
- Opportunity to add third party oontent (Wikipedia is the classic)
- Intelligent auto-complete

*Negatives:*

- False recognition
- Language differences
- Lack of value of automated content

##The good news- or pragmatic insight

*Code* is probably not yours to deliver.
There are _many_ APIs available, from computer vision on. Even emotion recognition

Cloudinary (imgIX competitor) automatically adds alt text via WP plugin

##What does the future hold?

Repetition and comparison are in a relatively good state.

We need to advance machine learning for explanation and association.

LUIS - Language Understanding Intelligence Services
[aka.ms/luis-api](http://aka.ms/luis-api)
