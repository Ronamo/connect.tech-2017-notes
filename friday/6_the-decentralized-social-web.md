#The Decentralized Social Web ( Keith J. Grant )

Intercontinental Exchange
CSS In Depth

Slides: [https://keithjgrant.com/talks/decentralized-web/#/](https://keithjgrant.com/talks/decentralized-web/#/)


##Why social networks

- Quick & easy signup
- Better UI
- Larger userbase
- Short-form content


##Problems

- Little control
- Don't own your data
- Requires account on each network
- Virtually no cross-compatibility

## Attempts to unseat FB/Twitter

- Buzz
- Ello
- Diaspora
- Ping
- Mastodon

Another social network clone is not the answer

##The "IndieWeb"

- Grassroots movement
- Formalized standards
- W3C specifications

##Return to the original vision of the web

##Multiple Pieces to the Puzzle

- Microformats
- Webmentions
- IndieAuth
- Micropub

[indieweb.org](http://indieweb.org)

##Put all content on *your* site

- Articles
- Notes
- Replies
- Likes
- Reposts

##Microformats

- Ensure that markup is machine-parseable
- - vcard, hcard, opengraph, etc.

(from  hereon, refer to slides)
