#Icons Behaving Badly ( Andrew Malek )

[malektips.com](http://malektips.com)
@malekontheweb

(Oh, dear God...he's wearing a fedora. m'presenter)

##What does the title concept even mean?

!!! see definition
> An icon is a pictrogram or ideogram displayed on a computer screen in order to help the user navigate a computer system or mobile device. The icon itself is a *quickly comprehensible* symbol of a software tool, function or a data file...

(brief list of examples of icons through OS history)

##Before computer icons

- Cave paintings
- Hieroglyphics (ideograms) - We lost this knowledge in the 5th century, regained it in the 19th.
- Road signs (highly semiotics)

##What makes a good icon?

- recognizable before activation whenever possible
- photorealism / skeuomorphism not required
- culturally sensitive and aware
- consistent within system
- explained as needed
- consistency in location and activation method
- should be TESTED.

Recognizable

- User has general idea of icon function
- Five second rule
- Avoids fear of unknown
- Very few are close to 100% recognizable...
- Recognition may depend on context

*Five second rule*:

> If it takes more than 5 seconds to think of an appropriate icon for something, it is unlikely that an icon can effectively communicate that meaning
- Aurora Haley, N/N Group

(example of passing icons)

*Hamburger menu study, 2015:*

- age 18-44 80.6%
- age 45-64 52.4%
- When labeled, 20% higher click rate

Every single variation that's more than the hamburger icon alone had higher clicks

##What about the floppy disk?

These things are obsolete, but people still widely recognize it...this recognition means it's still a useful, valid icon.

###Some more history...

- skeuomorphic - looks like real world

This is not necessary. We can simplify design. This removes visual clutter, conserves space, allows for more generic icon language

##Cultural sensitivity

- Mailboxes don't look the same worldwide
- Owls aren't always "wise"
- What about a white + on red background?
- Don't use a stop sign...road signs differ
- Don't use flags for language. The French flag doesn't work for Haiti or Quebec, does it? No. Also, it can be seen as nationalist.

[flagsarenotlanguages.com](http://flagsarenotlanguages.com)


