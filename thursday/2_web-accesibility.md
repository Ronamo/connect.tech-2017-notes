#Web Accesibility: Making Websites Usable for Everyone  - ( Will Jayroe, Sr. Solutions Architect + UX Strategist )

---

Sr. Solutions / UX for TravelPort
Presentation details: [jayroe.com/wax](http://jayroe.com/wax)
Slides: [https://docs.google.com/presentation/d/1lNN6N2mzPI5ZhK1v98MMp3C5FQpbl0q0dMQPMLhDsxU/edit](https://docs.google.com/presentation/d/1lNN6N2mzPI5ZhK1v98MMp3C5FQpbl0q0dMQPMLhDsxU/edit)

---

Audience closes eyes to listen to NVDA screenreader (on Delta site)

### LANGUAGE MATTERS

Word choice matters. It's not about being "politically correct." It's about respect. Use first-person to describe what a user *has*, not what a user *is*.

- midget vs. little person / short of stature
- wheelchair _bound_ vs wheelchair _user_
- Handicapped room vs. Accessible room

### Plan for accessibility.

- It's cheaper up front
- Retrofitting is hard and expensive
- Accessibility benefits "power users" too. (e.g., keyboard-only users)

### WCAG

- http://w3.org/TR/WCAG
- wide range of reqs and recs
- New version to be released soon
- WebAIM is an excellent consultancy / compliance verification

### Section 508 Compliance

- Federal compliance level.
- All airports and travel services must be compliant.

### WCAG 2.0 Conformance Levels

- It's impossible to meet all guidelines...subjective and dependent
- -- perceivable
- -- operable
- -- understandable
- -- robust
- Three levels of compliance (A-AAA)
- AAA is most restrictive, especially in terms of design and color

### Conformance Example

1.4.1 Use of Color (Level A) (Three examples of white on blue)
Contrast ratio of 2.6 is A-Fail
4.5 is AA-Pass
7 is AAA-Pass

### Areas of Accessibility

- Visual
- Audio
- Physical
- Cognitive

#### Visual Accessibility

For those with complete blindness, low vision, info processing disorders.

#### Challenges

- Structure (html)
- lack of meaningful labels and text
- poor fallbacks
- repetitive nonskippable items
- poorly designed forms

Reordering items with CSS is an issue.

!!!ASK ABOUT CSS GRID AND REORDERING

Use h1-h6 ONLY FOR SEMANTICS. DO NOT USE FOR STYLING
Add `<a href="#tag">Skip to Tag</a>` links
Non-text content must be perceivable.  `alt="attributes"` CAN be blank.  Adding non-related content for SEO purposes is a huge accessibility issue. This info should be descriptive to what the image is, and not related to other items. All info should be non-biased.
`<tables>` ARE FOR DATA ONLY. Very difficult to get "right" for screenreaders
Form validation should be relevant to the affected fields, not all lumped into a single position at the top of the form.

#### Audio
requires captions.

#### Physical

- Can you do everything without a mouse? With only a mouse?

#### Cognitive
- be consistent
- make it readable (e.g. open dyslexic font as an option)

---

Apple uses accessibility in their apps _first_ to assist with automated testing.  This is ground floor for them.

*SEE: "Tooling" slide*

### Baseline Testing and Success Criteria

1. Keyboard - end-to-end with keyboard only - 80% there
2. Screen Reader - 90% there

## YOU CANNOT MEET 100% COMPLIANCE

Add a usability statement to indicate your intent and show that you are at least attempting to offer compliance.

## SEE THIS:

### Exponential Design

Method card based system

[jayroe.com/exd](http://jayroe.com/exd)
