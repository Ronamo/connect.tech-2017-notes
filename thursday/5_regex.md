# Regular Expressions ( Jordan Kasper @jakerella )

```
/^[Rr]eg(ular\s)?[Ee]x(p|pressions?)?$/
```
[jordankasper.com/regex-101](http://jordankasper.com/regex-101)
---

Regex are string pattern matchers (duh)

Often used for data validation
Test against patterns (true/false)
Find matching groups (and possible replace content)

What language? (wrong question)
Which _engine_?

Example: PHP has both native, _and_ PERL compatible
POSIX (standard), BRE/ERE (Linux (grep)), Perl, java.util.regex, XRegExp (JS)

*All demos will be in XRegExp*

## What do they look like?

```
/the regex goes in here/
`the regex goes in here`
"the regex goes in here"
"/the regex goes in here/"
```

In other words, syntax changes, but pattern is the same.

## What do they _really_ look like?

JS

```
/foo/.text("foobar");
```

Here `/foo/` is a RegExp _object_ in JS

## Simple character test

```
/t/.test("Connect Tech"); // true
"Connect Tech".match(/t/); // ["t"] || null
```

For max efficiency, use `.test()` since it doesn't have any side effects

```
"Connect Tech".match(/n/); // ["n"]
"Connect Tech".match(/n/g); // ["n","n"]  because /g is global
```

For case insensitivity:

```
"Connect Tech".match(/t/ig); // ["t","T"]
```

### Flags

- `.` - any character
- `[]` - character class
- `[-]` - dash inside class indicates range (e.g. for hex: `/[a-f0-9]/`
- `?` - optionality
- `+` - 1 or more
- `*` - 0 or more
- `{}` - number range
- `{1}` - specific number (can use range in form `{6,8}` ). Must immediately follow character class or character
- `^` - negation (must be first character in `[]` class, eg. `[^a]` checks for _not a_
- `\` - escape following character if it is otherwise special
- `|` - alternation (or). Must be _within a `()` group_
- `(?:)` - non-matching group. `?:` must be first part of `()` group.
- `\t` - tab
- `\n` - newline
- `\v` - carriage return
- `\s` - ALL whitespace
- `\S` - anything _but_ whitespace
- `\d` - digit (easier than `[0-9]`)
- `\D` - not a digit
- `\w` - word (easier than `[a-zA-Z]`)
- `\W` - not word
- `^` - start of string
- `$` - end of string

### Replacement

Differers by engine and language

*JS*:

`String.replace(pattern,replacement)`
```
"apples".replace(/(p)?/, "A

## Resources

- [regex101.com](http://regex101.com)
- [regular-expressions.info](http://regular-expressions.info)
- [rexper.com](http://rexper.com) (Will produce railroad diagram)
- [regexcrossword.com](http://regexcrossword.com)

## Lookarounds

*Positive look ahead*

```
/(the)(?=\sfat)/i
```

*The* fat cat sat on the mat.

Only matches if the look ahead appears after the match.

*Negative lookahead*

```
/(the)(?!\sfat)/i
```

Only matches if the negative look ahead does not appear after the match.

The fat cat sat on *the* mat.

!!! Look up "look behinds" for regex (note they're not available in JS because of the huge memory drag)
