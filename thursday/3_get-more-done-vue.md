#Get More Done with Vue.js - ( Johnathan Kemp )

https://github.com/jkemp

## Get more done with Vue.js

- what is it
- easy to get started
- flexible
- fast
- devtools / tooling

## Intro Slides
- A Framework for building UI
- Which is the best Framework?
- Not the correct question. Whatever works best for your solution is the best option.
- Can be implemented incrementally
- Only 23kb for Vue2.0
- Powerful enough for SPA

## Templates

- Templates are written in HTML
- Data is bound via attributes
- Uses "mustache" syntax more or less
- Directives are used to provide specific behavior to HTML elements
- - Examples: `v-for="item in list"` to iterate
- Event listeners: `v-on`

## Components

- Abstract functionality into a "smaller" Vue instance
- These can be composed in HTML

## SKIPPING SOME STUFF

This guy's basically going over the documentation tutorial. Totally skippable.  Now for the meat.

## Simplicity

All you need is HTML5 and ES5 JS

### How does this compare?

Angular2 uses Typescript
React uses JSX but can be used without it...which gets ugly and time consuming if you're just learning it. It's all syntactic sugar for `React.createElement(...)`

#### Examples

*React*
Examples of react w & w/o JSX
ES2015+ - _not_ required, but it sure helps. All documentation nuses ES2015 syntax. This requires Babel to fully implement currently.

## Vue's Inspiratation - Familiarity is an advantage

Takes cues from Knockout, Backbone, etc.

## Comparison to React and Angular

- React & Vue both use virtual DOM, create view components, and work with external libraries easily.
- Angular v1 (angularjs) - Similar syntax, but Vue is simpler in API and design, less opinionated, and faster.
- Angularjs (Angular 2.0+) - Double size of Vue (w/ treeshaking!) but similar performance

- !!! vuejs/vue-loader <-- Official Webpack loader for single-file components.
- Render functions are avalable.
- JSX can be used as well.

## Single File Components and other things

- requires webpack
- can use all sorts of compilers
- Server-side rendering is _possible_ but not _trivial_. (Check official docs)
- NUXT.js simplifies SSR.
- WEEX - Like React Native, but for Vue.  (iOS, Android) (not super mature, but is under steady dev)

### Animation

- Has built-in transitions
- Can integrate 3rd party animation libraries

### Extendable

- custom directives
- mixins
- plugins

### Vue-Router / Vuex

Officially supported routing library
De facto state management library (comparable to Redux)

## CAVEAT

Vue _cannot_ detect property addition or deletion.
Solution is to declare all reactive properties up front and leave them empty.


## SEE:

- Offical docs and forum
- awesome-vue
- Laravel actually plays really nicely with Vue.
