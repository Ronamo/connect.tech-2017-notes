#Design & Interactivity Without Sacrificing Speed ( Abby Becker, AMP )

Good/cheap/fast, pick 2

Good - beautiful, awesome, performant webpage
Cheap - Cost of development

Currently, we can't have all three.

## The history of web design

- *1.0* - Ugly, and full of links. Large blocks of text.
- *2.0* - Prettier, faster, popups, ads, ads, ads
- *Current* - Think "Google doodles"

## Where we are

It's all pretty, and rich, and woo-woo, but it's slow as molasses.

We deal with content shifting, and FOUC all the time now.

This is exacerbated on mobile (this is shaping up to be an ad for AMP)

Current _average load time_ is *19 SECONDS*.


# WALKED OUT OF THIS TALK
This was 100% a product pitch for AMP, which is a net negative for the web for a huge variety of reasons.
