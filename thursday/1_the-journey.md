#The Journey - ( Brandy Porter @chickerbp )

##Administrative notes

- This is the fourth year of Connect.TECH
- 1,000+ attendees this year.
- 120 presentations
- *JazzCon* - NOLA, March 21-23
- *VUECONF* - NOLA, March

----

Brandy was Dir. of Design @ Big Nerd Ranch, became VP of Eng. (but why as designer?) - Was more about making the company grow and take care of the entire team. Next taught UX at General Assembly.

> "If life presents you with 8 paths, and none of them appeal to you, you must find the 9th path."

Literally living in a van for 6 months w/ BF and dog. Wanted to actually be "digital nomads" and push the limits of remote work.

>\#vanlife is awesome...but it has its challenges.

The US is nowhere near as connected as we'd like to think.

###Pyramid of User Needs
(Compare to Maslow's hierarchy of needs)

```
    Joy
  Usability
 Reliability
Functionality
```

Take risks...they're worth it...*but* also be sure you have a safety net / backup plan.

@chickerbp is now Director of Marketing for MailChimp.

Do what you're good at, and what you enjoy, and make a difference with it.
Treat life and work as a journey, and enjoy it. See the sights, enjoy every step, and deal with problems as they come along.

- @thefulcrumproject
- [fullcirclementors.com](http://fullcirclementors.com)
- [9thpathcreative.com](9thpathcreative.com)
