#Show Me How You REST ( Dustin Goodman )
*How we can be more RESTful*
[Slides](https://goo.gl/ebohvz)

@dustingoodman
[github.com/dustingoodman](http://github.com/dustingoodman)
Software Lead at Springbot
ATL AngularJS Co-Organizer

---

##What is REST?

1. Handling of HTTP verbs with REST and best practices
2. Response best practices & aerror messaging
3. Version controlling apis
4. Rate limiting requests
5. Batch requests
6. Q&A

##Representational State Transfer

Created by Roy Fielding in 200 as part of dissertation at UV Irvine
Arthictectural design principle of communicating data between to sources


##Guiding principles of REST

* Performance
* Scalability
* Uniform interface - Everything is predictable
* Ability to modify resource at any time - Must handle collisions from multiple requests
* Clear communication
* Reliability

##Classic REST v GraphQL

There are a few major differences in a true REST API and implemention GraphQL
GraphQL is RESTful by definition

*Key differences*

||REST||GraphQL||
|verbs | query keyword|
|endpoint is object identity | entity is separate from query|
|1 route handler per 1 request | 1 query can access many resources|

##HTTP
Web standard for communicating data
follows REST principles
uses a URI and an HTTP verb

##HTTP Verbs

* OPTIONS - metadata about allowed resource methods
* GET - retrieve document/set of documents
* HEAD - same as get without message body
* POST - create a new document of the specified resource
* DELETE - delete the specified document
* PUT - updates or creates the specified document (can supplant POST but requires full document)
* PATCH - partially update the specified document (requires ID and fields to be updated)

##Best Practices

Implementation should follow a well defined spec
[jsonapi.org](http://jsonapi.org) is a good starting place

Three rules for all requests:

1. Only performs specified verb
2. URIs should only be nouns (e.g., `/resource/id/` is good, `/resource/id/delete` is bad)
3. All inputs should be consistent

Example of bad: AdRoll (see slide)

Their verbs are just for show...end part of URI is a verb that isn't an HTTP verb

`PUT  /api/v1/campaign/pause` should be `PATCH /api/v1/campaign` and patch something within it to indicate status. This leaves us with a single endpoint instead of multiple

## Response Best Practices & Error Messaging

### Response Specifications

No formal spec aside from have a proper HTTP status code, and have only requested data.

### Collection seponse sample

*Response shouhld have:*

* links
* data (document)
* id
* type
* attributes

```
{
  "links": {
    "self" : "/path/to/articles"
  },
  "data" : [
    "type" : "articles",
    "id" : "1",
    "attributes" : {
      "title" : "Learn Rest",
      "author" : "Sum Dev"
    }
  ]
}
```

### Error response sample

* requires array of error objects
* This is loosely defined
* Code and title are optional fields chosen for illustration (see slide)

### Speed Bosts: Reduced Documents

* Response technique for improving API speed
* Used by both Google and Facebook
* Response only contains basic document information
* Must use request parameters to get more info

*Example*

```
GET /1.0/123asdf
// vs
GET /1.0/123asdf?fields=name
```

### Case Study (docs and versioning): Facebook Graph API

* [developers.facebook.com/docs/apps/changelog](http://developers.facebook.com/docs/apps/changelog)
* v1 to v2 introduced major breaking changes as entire sub-APIs were removed
* minor versions bumps still include bug fixes
* Must specify version in request

### Security Blanket

* Prevent users from unintentional harmful behavior
* Options for implementation
* *  Time based request limits (least beneficial...can screw non-malicious users)
* * Concurrency limits
* * Account Limits
* * IP limitations
* Caveat: offer options for combining single requests

#### Case Study: MailChimp

* No daily limits
* 10 call concurrency limitations per access key
* * All others are blocked
* API key tied to account so easily monitored
* If you have multiple access keys, allows you to run calls for all of them simultaenously but only 10 per access key
* Can group calls by passing correct params

Has issues because it just limits you to email creation

#### Case Study: Google Analytics

* Default 50k request per API project per day
* * Can request more but policies must be met
* 10 requests per second per IP address
* Prevents full scale DDOS because of daily limits while offering distributed calls system to allow for scaling
* Can group calls by defining data you want back in request


#### Case Study: Facebook

* 200 calls / person / hour
* 4800 calls / person / 24 hours
* person is an access token
* no offering for reducing calls by groups

For sub-keys? Users of a specific FB related app

### Batch Requests - Why batch?

* Need to perform several operations on a set of objects
* Reduces calls to API
* * Extremely helpful if rate limits exist
* By definition, batches are slow
* They are not technically RESTful

