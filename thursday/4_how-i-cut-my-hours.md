# How I Cut my Working Hours in Half and Still Managed to Get More Done ( Jason Lengstorf )

Lunchtime keynote, Thursday

[Slides](http://git.io/v9L9Z)

---

Was working 70-90 hours per weeko

Landed a HUGE contract (a "black friday" project...site for a Fortune 500 that needed a marketing site)
Worked 100+ hours _that week alone_

The client loved it, and made huge ROI, plus Jason earned a bundle.  It also won an Addy.
But...missed Thanksgiving w/ family, but slept less than 4 hours per night, at fast food delivery for EVERY meal. His beard even actually fell out!

> Poor and healthy is better than successful and sick.

Cut back on *everything*

- no calls outside of business
- no notifications
- no phone during social time

If it was down to being a person or having a job, personship was better.

But...it didn't work out as expected.

What actually happened:

- Nobody noticed
- business grew
- he started getting more donw by working fewer hours.

Studies show:
- *60+* hours _absolutely makes you *less* productive_ than at 40 hours / week.
- Long hours increase employee turnover
- Decreases passion and engagement when workers are overworked.
- Creativity suffers if we don't take time to go outside and be people.
- We lose 20% of our time *per task* while multitasking
- Sleeping less than 7 hours imparis us as much as being too drunk to drive (0.08 breathalyzer)


## 5 Strategies

- track time (passively)
- time-box tasks
- front-load important work
- prioritize aggressively
- stop multitasking

### 1 Track your time

If you acknowledge it, you address it.
Tools: RescueTime / Timing / Thyme

### 2 Time-box your tasks

You're never more productive than when you're up against a deadline with 13% battery.

#### Find your Rhtyhm

- decide how long you want to focus for (think Pomodoro)
- Close and turn off _everything_ unrelated. EVERYTHING. *EVERYTHING*.
- Start timer
- Unleash the beast
- Take the break when the timer ends. TAKE IT.
- Lather, rinse, repeat

This works because you're *not distracted*.
We enter a *flow state* (deep hack mode)
We're *taking breaks*...this is good for us. If we don't, we become "code zombies"
We're *still available*


### 3 Front-load important work

We are the best versions of ourselves early in our day.

### 4 Prioritize Aggressively

If everything is The Most Important Thing™, nothing is.
_Multiple_ priorities don't exist. This is marketing doublespeak from the 1980's.

Put tasks into a tournament bracket. Prioritize.

### 5 NEVER MULTITASK

Context switching is FATAL to productivity
We _feel_ productive when multitasking, but single-tasking actually _makes us productive_.
